/*En la joyer�a Mis perlas, necesitan un programa que les permita calcular el descuento 
del 10% a todas las ventas que realicen. Han pedido recomendaci�n y encontraron
 que usted es la persona m�s adecuada para programarlo. Por lo tanto le han pedido 
que realice una aplicaci�n en java para calcular el 10% de la venta. Tanto la entrada 
como la salida h�galo en un cuadro de di�logo. Use la clase JOptionPane.*/
import javax.swing.JOptionPane;
public class Joyeria{
  public static void main(String arg[]){
    double descuento, venta, precioAPagar;
    String datoLeido;
    datoLeido=JOptionPane.showInputDialog("Digite el monto de la venta");
    venta=Double.parseDouble(datoLeido);
    descuento=venta*0.01;
    precioAPagar=venta-descuento;
    JOptionPane.showMessageDialog(null,"El total a pagar es "+precioAPagar);
    
  }//Fin del main
}//Fin de la clase