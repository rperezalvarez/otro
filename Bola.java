	
	public class Bola {
		
		private int numero;
		private String estilo;
		
		public Bola(){
			numero = 1;
			estilo = "";
		}
		
		public void setNumero(int numero){
			this.numero=numero;
		}
		
		public int getNumero(){
			return numero;
		}
		
		public void setEstilo(String estilo){
			this.estilo=estilo;
		}
		
		public String getEstilo(){
			return estilo;
		}
	}
	
