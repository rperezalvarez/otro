
public class Comunidad{
	
	public static void main(String args[]){
		
		int numero = 0;
		Persona vecino1 = new Persona();
		Persona vecino2 = new Persona();

		
		vecino1.setNombre("Roberto");
		vecino1.setFechaNacimiento("27-05-1990");
		
		vecino2.setNombre("Lucia");
		vecino2.setFechaNacimiento("05-03-2000");
		
		System.out.println("Nombre vecino 1: " + vecino1.getNombre());
		System.out.println("Nombre vecino 2: " + vecino2.getNombre());
	}
}
